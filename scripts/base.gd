extends Node2D

var food
var apple_pies

func _ready():
	food = 0
	apple_pies = 0

func increase_food(amount):
	food += amount
	get_node("../ApplesLabel").set_text("Apples: " + str(food))

func decrease_food(amount):
	food -= amount
	get_node("../ApplesLabel").set_text("Apples: " + str(food))

func increase_apple_pies(amount):
	apple_pies += amount
	get_node("../ApplePiesLabel").set_text("Apple Pies: " + str(apple_pies))

func decrease_apple_pies(amount):
	apple_pies -= amount
	get_node("../ApplePiesLabel").set_text("Apple Pies: " + str(apple_pies))
