extends Node2D

var currentPosition
var direction
var reached_target = false
var v_five = Vector2(5, 5)
var target = self.position
var tasks = []
var current_task
var current_state
var food = 0

var carrying_capacity = 2

const STATE = {
	IDLE = 0,
	GATHERING_FOOD = 1,
	MAKING_APPLE_PIES = 2
}

const TASK = {
	FIND_NEAREST_FOOD_LOCATION = 0,
	COLLECT_GOODS = 1,
	STORE_GOODS = 2,
	MAKE_APPLE_PIE = 3
}

var task_completed

func make_apple_pie():
	var base = get_node("../Base") 
	if base.food < 4:
		gather_food()
		tasks.push_front(TASK.MAKE_APPLE_PIE)
		return
	current_task = STATE.MAKING_APPLE_PIES
	base.decrease_food(4)
	base.increase_apple_pies(1)

func gather_food():
	current_state = STATE.GATHERING_FOOD
	tasks.push_front(TASK.FIND_NEAREST_FOOD_LOCATION)
	tasks.push_front(TASK.COLLECT_GOODS)
	tasks.push_front(TASK.STORE_GOODS)

func find_nearest_food_location():
	var food_resources = get_node("/root/senpai").food_resources
	
	if food_resources.size() == 0:
		return null
	
	var my_position = self.position
	var temp_distance
	var nearest_distance = my_position.distance_to(food_resources[0].position)
	var nearest_food_location = food_resources[0].position
	for food_resource in food_resources:
		temp_distance = my_position.distance_to(food_resource.position)
		if temp_distance < nearest_distance:
			nearest_distance = temp_distance
			nearest_food_location = food_resource.position
	return nearest_food_location

func _ready():
	self.set_process(true)
	self.target = self.position
	make_apple_pie()

func _process(delta):
	
	if current_task == null:
		current_task = tasks.pop_back()
	
	if current_task == TASK.FIND_NEAREST_FOOD_LOCATION:
		target = find_nearest_food_location()
	
	if current_task == TASK.COLLECT_GOODS and reached_target:
		food = get_node("../Food").decrease_food(carrying_capacity)
		target = get_node("../Base").position
	
	if current_task == TASK.STORE_GOODS and reached_target:
		get_node("../Base").increase_food(food)
	
	if current_task == TASK.MAKE_APPLE_PIE and reached_target:
		make_apple_pie()
	
	currentPosition = self.position
	
	if current_task == null:
		make_apple_pie()
	
	if target == null:
		return
	
	reached_target = false
	
	if currentPosition >= target - v_five and currentPosition <= target + v_five :
		reached_target = true
		current_task = tasks.pop_back()

	if reached_target == false:
		direction = target - currentPosition
		self.position = currentPosition + direction.normalized() * 100 * delta
