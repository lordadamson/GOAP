extends Node2D

var food

func _ready():
	food = 40
	get_node("/root/senpai").register_food_resource(self)

func decrease_food(amount):
	food -= amount
	return amount
